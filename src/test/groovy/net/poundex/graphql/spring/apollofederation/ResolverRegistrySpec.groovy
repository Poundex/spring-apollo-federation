package net.poundex.graphql.spring.apollofederation

import org.dataloader.DataLoader
import org.dataloader.DataLoaderRegistry
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import spock.lang.Specification
import spock.lang.Subject
import testdata.ResolverOne
import testdata.ResolverThree
import testdata.ResolverTwo

class ResolverRegistrySpec extends Specification {
	
	ResolverOne resolverOne = new ResolverOne()
	ResolverTwo resolverTwo = new ResolverTwo()
	ResolverThree resolverThree = new ResolverThree()
	
	@Subject
	ResolverRegistry resolverRegistry
	
	void setup() {
		resolverRegistry = new ResolverRegistry([TypeOne: resolverOne, TypeTwo: resolverTwo, TypeThree: resolverThree])
	}
	
	void "Return empty Mono for unknown type"() {
		when:
		Mono<FederationReferenceResolver<?, ?>> ret = resolverRegistry.getResolver("TypeFour")
		
		then:
		StepVerifier.create(ret).expectNextCount(0).verifyComplete()
	}

	void "Return the resolver for the provided type"() {
		when:
		Mono<FederationReferenceResolver<?, ?>> ret = resolverRegistry.getResolver("TypeOne")

		then:
		StepVerifier.create(ret).expectNext(resolverOne).verifyComplete()
	}
	
	void "Register DataLoader for all Resolvers in DataLoaderRegistry"() {
		given:
		DataLoaderRegistry dataLoaderRegistry = Mock(DataLoaderRegistry)
		
		when:
		resolverRegistry.registerLoadersIn(dataLoaderRegistry)
		
		then:
		1 * dataLoaderRegistry.register("TypeOne", _ as DataLoader)
		1 * dataLoaderRegistry.register("TypeTwo", _ as DataLoader)
		1 * dataLoaderRegistry.register("TypeThree", _ as DataLoader)
	}
}
