package net.poundex.graphql.spring.apollofederation

import org.springframework.beans.factory.ObjectProvider
import spock.lang.Specification
import testdata.ResolverOne
import testdata.ResolverThree
import testdata.ResolverTwo

class FederationAutoConfigurationSpec extends Specification {
	
	void "Create ResolverRegistry with all provided FederationReferenceResolver beans"() {
		given: "Some discovered resolvers, and an ObjectProvider that provides them"
		ResolverOne resolverOne = new ResolverOne()
		ResolverTwo resolverTwo = new ResolverTwo()
		ResolverThree resolverThree = new ResolverThree()
		
		ObjectProvider<FederationReferenceResolver<?, ?>> objectProvider = Stub(ObjectProvider) {
			stream() >> [resolverOne, resolverTwo, resolverThree].stream()
		}
		
		and: "An instance"
		FederationAutoConfiguration federationAutoConfiguration = new FederationAutoConfiguration()
		
		when: "Providing the ResolverRegistry instance"
		ResolverRegistry resolverRegistry = federationAutoConfiguration.resolverRegistry(objectProvider)
		
		then: "The Resolvers are registered in the registry"
		resolverRegistry.typeToResolverMap.size() == 3
		resolverRegistry.typeToResolverMap["TypeOne"].is(resolverOne)
		resolverRegistry.typeToResolverMap["TypeTwo"].is(resolverTwo)
		resolverRegistry.typeToResolverMap["TypeThree"].is(resolverThree)
	}
}
