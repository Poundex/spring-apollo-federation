package testdata;

import net.poundex.graphql.spring.apollofederation.FederationReferenceResolver;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Set;

public class ResolverOne implements FederationReferenceResolver<ResolverOne.TypeOne, Long> {

    @Override
    public Mono<Map<Long, TypeOne>> resolveReferences(Set<Long> keys) {
        return null;
    }

    @Override
    public Long extractKey(Map<String, Object> args) {
        return null;
    }
    
    public static class TypeOne { }
}
