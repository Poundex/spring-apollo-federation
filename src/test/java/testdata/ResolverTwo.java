package testdata;

import net.poundex.graphql.spring.apollofederation.FederationReferenceResolver;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Set;

public class ResolverTwo implements FederationReferenceResolver<ResolverTwo.TypeTwo, Long> {

    @Override
    public Mono<Map<Long, TypeTwo>> resolveReferences(Set<Long> keys) {
        return null;
    }

    @Override
    public Long extractKey(Map<String, Object> args) {
        return null;
    }
    
    public static class TypeTwo { }
}
