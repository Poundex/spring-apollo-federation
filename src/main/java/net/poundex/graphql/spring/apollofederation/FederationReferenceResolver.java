package net.poundex.graphql.spring.apollofederation;

import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Set;

public interface FederationReferenceResolver<T, K> {
    
    Mono<Map<K, T>> resolveReferences(Set<K> keys);
    K extractKey(Map<String, Object> args);
}
