package net.poundex.graphql.spring.apollofederation;

import com.apollographql.federation.graphqljava.Federation;
import com.apollographql.federation.graphqljava._Entity;
import graphql.GraphQLContext;
import graphql.TypeResolutionEnvironment;
import graphql.execution.DataFetcherResult;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLObjectType;
import lombok.extern.slf4j.Slf4j;
import org.dataloader.DataLoaderRegistry;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.GenericTypeResolver;
import org.springframework.graphql.boot.GraphQlSourceBuilderCustomizer;
import org.springframework.graphql.web.WebInterceptor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Configuration
@Slf4j
public class FederationAutoConfiguration {

    @Bean
    public ResolverRegistry resolverRegistry(ObjectProvider<FederationReferenceResolver<?, ?>> referenceResolvers) {
        return new ResolverRegistry(referenceResolvers.stream()
                .collect(Collectors.toMap(
                        this::getTypeName,
                        v -> v)));
    }

    private String getTypeName(FederationReferenceResolver<?, ?> k) {
        return GenericTypeResolver.resolveTypeArguments(k.getClass(), FederationReferenceResolver.class)[0]
                .getSimpleName();
    }
    
    @Bean
    public GraphQlSourceBuilderCustomizer schemaFactoryCustomiser(ResolverRegistry resolverRegistry) {
        return builder -> builder.schemaFactory((typeDefinitionRegistry, runtimeWiring) ->
                Federation.transform(typeDefinitionRegistry, runtimeWiring)
                        .fetchEntities(env -> fetchEntities(env, resolverRegistry))
                        .resolveEntityType(FederationAutoConfiguration::resolveEntityType)
                        .build());
    }

    private <T, K> CompletableFuture<List<DataFetcherResult<T>>> fetchEntities(
            DataFetchingEnvironment env,
            ResolverRegistry resolverRegistry) {

        return Flux.fromIterable(env.<List<Map<String, Object>>>getArgument(_Entity.argumentName))
                .flatMapSequential(args -> {
                    String typeName = args.get("__typename").toString();
                    env.getGraphQlContext().put("__typename", typeName);

                    return resolverRegistry.<T, K>getResolver(typeName)
                            .flatMap(r ->
                                    Mono.fromFuture(env.<K, T>getDataLoader(typeName).load(r.extractKey(args))))
                            .map(ret -> DataFetcherResult.<T>newResult().data(ret).build())
                            .defaultIfEmpty(DataFetcherResult.<T>newResult().build());
                })
                .collectList()
                .toFuture();
    }

    private static GraphQLObjectType resolveEntityType(TypeResolutionEnvironment env) {
        return ((GraphQLContext) env.getContext()).<String>getOrEmpty("__typename")
                .map(tn -> env.getSchema().getObjectType(tn))
                .orElse(null);
    }
    
    @Bean
    public WebInterceptor dlrInterceptor(DataLoaderRegistry dataLoaderRegistry) {
        return (webInput, next) -> {
            webInput.configureExecutionInput((executionInput, builder) -> 
                    builder.dataLoaderRegistry(dataLoaderRegistry).build());
            return next.handle(webInput);
        };
    }
    
    @Bean
    public DataLoaderRegistry dataLoaderRegistry(ResolverRegistry resolverRegistry) {
        return resolverRegistry.registerLoadersIn(new DataLoaderRegistry());
    }
}