package net.poundex.graphql.spring.apollofederation;

import lombok.RequiredArgsConstructor;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderFactory;
import org.dataloader.DataLoaderOptions;
import org.dataloader.DataLoaderRegistry;
import org.dataloader.annotations.VisibleForTesting;
import reactor.core.publisher.Mono;

import java.util.Map;

@RequiredArgsConstructor
public class ResolverRegistry {
    private final Map<String, FederationReferenceResolver<?, ?>> typeToResolver;
    
    @SuppressWarnings("unchecked")
    public <T, K> Mono<FederationReferenceResolver<T, K>> getResolver(String typeName) {
        return Mono.justOrEmpty((FederationReferenceResolver<T, K>) typeToResolver.get(typeName));
    }

    public DataLoaderRegistry registerLoadersIn(DataLoaderRegistry dataLoaderRegistry) {
        typeToResolver.forEach((k, v) -> 
                dataLoaderRegistry.register(k, createDataLoader(k, v)));
        return dataLoaderRegistry;
    }
    
    private <T, K> DataLoader<K, T> createDataLoader(String typeName, FederationReferenceResolver<T, K> resolver) {
        return DataLoaderFactory.newMappedDataLoader((keys, environment) ->
                        resolver.resolveReferences(keys).toFuture(),
                DataLoaderOptions.newOptions().setCachingEnabled(false));
    }

    Map<String, FederationReferenceResolver<?, ?>> getTypeToResolverMap() {
        return typeToResolver;
    }
}
